package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TwoThreeTree<K, V> extends BTree<K, V>{
	
	public TwoThreeTree(Comparator<K> keyComparator) {
		super(keyComparator);
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public V get(K key) {
		return this.getAux(key, this.root);
	}
	
	private V getAux(K key, TreeNode N) {
		if (N == null || key == null) {
			return null; // not found
		}
		else {
			for (MapEntry M : N.entries) {
				int comparison = this.keyComparator.compare(key, M.key);
				if (comparison == 0) {
					return M.value;
				}
				else if (comparison < 0) {
					return this.getAux(key, N.left);
				}
				else {
					return this.getAux(key, N.center);
				}
			}
			return null;
		}
	}

	@Override
	public V put(K key, V value) {
		if (this.root == null) {
			MapEntry M = new MapEntry(key, value, keyComparator);
			this.root = new TreeNode(M, null, keyComparator);
			this.currentSize++;
			return value;
		}
		else {
			return this.putAux(key, value, this.root);
		}
	}
	
	private V putAux(K key, V value, TreeNode N) {
		
		if (this.isLeaf(N)) {
			N.entries.add(new MapEntry(key, value, keyComparator));
			this.currentSize++;
			this.split(N);
		}
		
		int comparison = this.keyComparator.compare(key,  N.entries.first().key);
		if (comparison < 0) {
			// left
			if (N.left == null) {
				N.left.entries.add(new MapEntry(key, value, keyComparator));
				this.currentSize++;
				return value;
			}
			else {
				return this.putAux(key, value, N.left);
			}
		}
		else if (comparison == 0) {
			// center
			if (N.center == null) {
				N.center.entries.add(new MapEntry(key, value, keyComparator));
				this.currentSize++;
				return value;
			}
			else {
				return this.putAux(key, value, N.center);
			}		
		}
		else {
			// center
			if (N.right == null) {
				N.right.entries.add(new MapEntry(key, value, keyComparator));
				this.currentSize++;
				return value;
			}
			else {
				return this.putAux(key, value, N.right);
			}		
		}
	}

	@Override
	public V remove(K key) {
		if (key == null) {
			return null;
		}
		if (this.contains(key)) {
			return this.removeAux(key, root);
		}
		
		return null;
	}
	
	private V removeAux(K key, TreeNode N) { // doesn't work
		if (N == null) {
			return null; // not found
		}
		else {
			for (MapEntry M : N.entries) {
				int comparison = this.keyComparator.compare(key, M.key);
				if (comparison == 0) {
					M.deleted = true;
					this.currentSize--;
					this.deleted++;
					return M.value;
				}
				else if (comparison < 0) {
					return this.getAux(key, N.left);
				}
				else {
					return this.getAux(key, N.center);
				}
			}
			return null;
		}
	}

	@Override
	public boolean contains(K key) {
		return this.get(key) != null;
	}
	
	@Override
	public List<K> getKeys() { // doesn't work
		List<K> result = new ArrayList<>(this.size());
		int i = 0;
		this.getKeysAux(this.root, result);
		return result;
	}

	private void getKeysAux(TreeNode N, List<K> result) { // doesn't work
		if (N == null) {
			return;
		}
		else {
			this.getKeysAux(N.left, result);
			for (int i = 0; i < this.size(); i++) {
				result.add(N.entries.get(i).key);
			}
			this.getKeysAux(N.center, result);
			this.getKeysAux(N.right, result);
		}
	}

	@Override
	public List<V> getValues() { // doesn't work
		List<V> result = new ArrayList<>(this.size());
		this.getValuesAux(this.root, result);
		return result;
	}

	private void getValuesAux(TreeNode N, List<V> result) { // doesn't work
		if (N == null) {
			return;
		}
		else {
			this.getValuesAux(N.left, result);
			for (int i = 0; i < this.size(); i++) {
				result.add(N.entries.get(i).value);
			}
			this.getValuesAux(N.center, result);
			this.getValuesAux(N.right, result);
		}
	}

	@Override
	boolean isLeaf(TreeNode N) {
		return N.left == null && N.center == null && N.right == null;
	}

	@Override
	void split(TreeNode N) {
		if (N == null) {
			return;
		}
		
		if (N.entries.size() > 2) {
			N.left.entries.add(N.entries.get(0));
			N.center.entries.add(N.entries.get(2));
			N.temp.entries.add(N.entries.get(1));
			
			N.entries = N.temp.entries;
		}
		
		else {
			this.split(N.parent);
		}
		
	}

}
