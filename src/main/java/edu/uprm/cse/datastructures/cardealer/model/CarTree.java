package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

public class CarTree {

	public static TwoThreeTree<Long, Car> carTree = new TwoThreeTree<>(new KeyComparator());
	
	public static TwoThreeTree<Long, Car> getInstance() {
		return carTree; // return a new instance of the TwoThreeTree carTree
	}
	
	public static void resetCars() {
		carTree = new TwoThreeTree<>(new KeyComparator()); // clears the elements on the TwoThreeTree carTree
	}
	
}
