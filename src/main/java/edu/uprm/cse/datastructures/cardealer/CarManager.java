package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarTree;
import edu.uprm.cse.datastructures.cardealer.util.TwoThreeTree;

@Path("/cars")
public class CarManager {

	// call a new instance of the list
	private static TwoThreeTree<Long,Car> carTree = CarTree.getInstance();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getCarList(){ // returns all the elements available in the list
		Car[] carArray = new Car[carTree.size()];
		if (carTree.isEmpty()) {
			return carArray;
		}

		for (int i = 0; i < carTree.size(); i++) { // move all cars in the hashtable into the array
			carArray[i] = carTree.getValues().get(i);
		}
		return carArray;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id){ // returns the element with the specified id value
		if (carTree.get(id) != null) {
			return carTree.get(id);
		}
		throw new NotFoundException();
	}

	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car){ // adds a new element to the list
		carTree = CarTree.getInstance();
		if (!carTree.contains(car.getCarId())) {
			carTree.put(car.getCarId(), car);
			return Response.status(201).build();
		}
		return Response.status(404).build(); // if the car already exists, return 404 (Error)

	}

	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(Car car){ // updates an existing element with the specified id
		if (carTree.contains(car.getCarId())) {
			carTree.remove(car.getCarId());
			carTree.put(car.getCarId(), car);
			return Response.status(Response.Status.OK).build();
		}
		return Response.status(404).build(); // if the id does not exist, return 404 (Error)
	}

	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id){ // deletes an existing element with the specified id
		if (carTree.contains(id)) {
			carTree.remove(id);
			return Response.status(Response.Status.OK).build();
		}
		return Response.status(404).build(); // if the id does not exist, return 404 (Error)
	}	
	
}